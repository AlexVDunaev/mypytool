# -*- coding: utf-8 -*-
"""
Created on Tue Apr 26 14:07:27 2022

@author: Dunaev


Описание:
Небольшой утилитарный класс, который предоставляет функции приема/отправки
сообщений по TCP/UDP в режиме клиент/сервер.
Для каждого варианта транспорта и роли свой поток приема данных с небольшими вариациями

Клиент постоянно пытается соединиться с указанным хостом, а
сервер на старте биндит порт и слушает его.
Для всех вариантов доступен авто-ответ (если колбэк пользователя возвращает буфер
этот буфер отправляется клиенту отправившему запрос).


Использование:
obj = xNetTool("127.0.0.1", 7777, 1, RxHandlerServer, ServiceHandler, transport="TCP", role="server")
Для сервера важен порт, который он будет слушать. 
RxHandlerServer, ServiceHandler - это события, куда будут приходить данные и 
служебные сообщения.
def RxHandlerServer(userid, sock, data, fromip):
    data - данные которые получены
    fromip - кортеж ip:port клиента
Если на принятые данные нужно отправить ответ, его нужно вернуть через retun,
например:  
    return "OK".encode('UTF-8')


# transport = ["TCP" | "UDP"]
# role = ["server" | "client"]
    

вызов метода senddata():
- Для TCP(server): выдача сообщения всем подключенным клиентам.
- Для UDP(server): выдача сообщения на адрес клиента, от которого
получено последнее сообщение.
- Для роли(client): выдача сообщения на (host, port), указанный при создании объекта

"""

import socket
from time import sleep
import logging
from   threading import Thread
import select


#  Класс-обертка для создания потоков
class ctrlThread(Thread):
    def __init__(self, name, func):
        Thread.__init__(self)
        self.name = name
        self.func = func
    def __del__(self):
        pass
    def run(self):
        self.func()


# xNetTool("127.0.0.1", 7777, 1, RxHandlerServer, ServiceHandler, transport= "TCP", role= "server")
class xNetTool:
    # return FALSE: sock down, True : sock up
    def sockReading(self, sock):
        sock.settimeout(1)
        # reading...                    
        while self.doExit == 0:
            try:
                (data, fromip) = sock.recvfrom(1024)
                if not data:
                    sock.close()
                    return False
                else:
                    (host, port) = fromip
                    self.fromip = None
                    if type (port) == int:
                        if port > 0:
                            self.fromip = fromip
                    answer = self.rxEvent(self.userid, sock, data, fromip)
                    if answer is not None:
                        if self.fromip is not None:
                            sock.sendto(answer, fromip)
                        else:
                            sock.send(answer)
                            
            except socket.timeout:
                logging.debug("sockReading>timeout[%s:%d]..." % (self.host, self.port))
        return True
    
    
    def tcpClientThread(self):
        def closeSock(self, sock):
            self.sock = []
            sock.close()
            if self.serviceEvent:
                self.serviceEvent(self.userid, (self.host, self.port), 'close')
            pass
        while self.doExit == 0:
            isConnect = False
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.setblocking(False)
            # connect / reconnect
            while self.doExit == 0 and isConnect == False:
                try:
                    logging.debug("tcpClientThread>[%s:%d] try connect..." % (self.host, self.port))
                    sock.settimeout(3)
                    sock.connect((self.host, self.port))
                    # Чтение сокета в цикле while. Выход если разрыв соединения
                    self.sock = [sock]
                    if self.serviceEvent:
                        self.serviceEvent(self.userid, (self.host, self.port), 'connect')
                    isConnect = self.sockReading(sock)
                except socket.error as error:
                    isConnect = False
                    logging.error("tcpClientThread>[%s:%d]: Error[%s]" % (self.host, self.port, error))
                    closeSock(self, sock)
                    sleep(0.5)
                    break
                except socket.timeout:
                    logging.debug("tcpClientThread>[%s:%d] connect timeout" % (self.host, self.port))
            if isConnect:
                closeSock(self, sock)
            
        # done
        logging.debug("tcpClientThread>[%s:%d].done" % (self.host, self.port))
        pass

    def tcpServerThread(self):
        def closeSock(self, sock):
            peername = sock.getpeername()
            inputs.remove(sock)
            self.sock.remove(sock)
            sock.close()
            if self.serviceEvent:
                self.serviceEvent(self.userid, peername, 'close')
            pass

        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.setblocking(False)
        server.bind((self.host, self.port))
        server.listen(5)
        inputs = [server]
        outputs = []
        self.sock = []
        while inputs and self.doExit == 0:
            # Ожидание: таймаут 1с
            readable, writable, exceptional = select.select(inputs, outputs, inputs, 1)
            for sock in readable:
                if sock is server:
                    connection, client_address = sock.accept()
                    connection.setblocking(0)
                    inputs.append(connection)
                    self.sock.append(connection)
                    if self.serviceEvent:
                        self.serviceEvent(self.userid, connection.getpeername(), 'accept')
                    
                else:
                    try:
                        data = sock.recv(1024)
                        if data:
                            answer = self.rxEvent(self.userid, sock, data, sock.getpeername())
                            if answer is not None:
                                sock.send(answer)
                        else:
                            logging.debug("tcpServerThread>[%s:%d] client down" % (self.host, self.port))
                            closeSock(self, sock)
                    except socket.error as error:
                        logging.error("tcpServerThread>[%s:%d]: Error[%s]" % (self.host, self.port, error))
                        closeSock(self, sock)

        # Закрою сокеты в обратном порядке их открытия
        inputs.reverse()
        for s in inputs:
            s.close()
        logging.debug("tcpServerThread>[%s:%d].done" % (self.host, self.port))
        pass
    
    def udpClientThread(self):
        logging.debug("udpClientThread>[%s:%d]." % (self.host, self.port))
        while self.doExit == 0:
            sock = socket.socket(socket.AF_INET,  socket.SOCK_DGRAM) 
            sock.setblocking(False)
            try:
                # udp connect (просто связывание сокета и порта назначения)
                sock.connect((self.host, self.port))
                self.sock = [sock]
                if self.sockReading(sock):
                    self.sock = []
                    sock.close()
            except socket.error as error:
                logging.error("udpClientThread>[%s:%d]: Error[%s]" % (self.host, self.port, error))
                self.sock = []
                sock.close()
                sleep(0.5)
        logging.debug("udpClientThread>[%s:%d].done" % (self.host, self.port))
        pass    
    
    def udpServerThread(self):
        def closeSock(self, sock):
            self.sock = []
            sock.close()
            if self.serviceEvent:
                self.serviceEvent(self.userid, (self.host, self.port), 'close')
            pass
                
        logging.debug("udpServerThread>[%s:%d]." % (self.host, self.port))
        while self.doExit == 0:
            sock = socket.socket(socket.AF_INET,  socket.SOCK_DGRAM) 
            sock.setblocking(False)
            try:
                sock.bind((self.host, self.port))
                self.sock = [sock]
                if self.sockReading(sock) == True:
                    closeSock(self, sock)
            except socket.error as error:
                logging.error("udpServerThread>[%s:%d]: Error[%s]" % (self.host, self.port, error))
                closeSock(self, sock)
                sleep(0.5)
                
        # done
        logging.debug("udpServerThread>[%s:%d].done" % (self.host, self.port))
        pass    
    
    
    def __init__(self, host, port, userid, rxEvent, serviceEvent, transport = "TCP", role = "server"):
        self.host = host
        self.port = port
        self.rxEvent = rxEvent
        self.userid = userid
        self.serviceEvent = serviceEvent
        self.transport = transport
        self.role = role
        self.doExit = 0
        self.sock = None
        self.fromip = None
        logging.debug("xNetTool.Init[%s:%d]." % (host, port))    
        
        if transport == "TCP" and role == "server":
            ctrlFunc = self.tcpServerThread
            
        if transport == "TCP" and role == "client":
            ctrlFunc = self.tcpClientThread
            
        if transport == "UDP" and role == "server":
            ctrlFunc = self.udpServerThread

        if transport == "UDP" and role == "client":
            ctrlFunc = self.udpClientThread
            
        self.thread = ctrlThread("%s.%s[%s:%d]" % (transport, role, host, port), ctrlFunc)
        self.thread.start()        
        
    def __del__(self):
        logging.debug("xNetTool.DEL[%s:%d]." % (self.host, self.port))
        if self.doExit == 0:
            self.close()

    def getRole(self):
        return self.role

    def getTransport(self):
        return self.transport

    def getHost(self):
        return self.host

    def getPort(self):
        return self.port

    def close(self):
        logging.debug("xNetTool.Close[%s:%d]..." % (self.host, self.port))
        self.doExit = 1
        self.thread.join(5)
        logging.debug("xNetTool.Close[%s:%d]...Done" % (self.host, self.port))
        
    def senddata(self, data):
        logging.debug("xNetTool.senddata[%s:%d]..." % (self.host, self.port))
        if self.sock is not None:
            for sock in self.sock:
                fromip = self.fromip
                try:
                    if fromip is not None:
                        sock.sendto(data, fromip)
                    else:
                        sock.sendall(data)
                except socket.error as error:
                    logging.error("senddata[%s:%d]: Error[%s]" % (self.host, self.port, error))

## ----------------------------------------------------------------------------

def democode():
    # Настройка логера
    logging.root.handlers = []
    
    logging.basicConfig(
         filename='xNet.log',
         level=logging.DEBUG,
         # level=logging.WARNING,
         format= '[%(asctime)s] %(levelname)s - %(message)s',
         datefmt='%H:%M:%S'
     )    
    def RxHandlerServer(userid, sock, data, fromip):
        print("Request[id:%d]>" % userid, data.decode('UTF-8'))
        return "AnswerToClient".encode('UTF-8')

    def RxHandler(userid, sock, data, fromip):
        print("Client Rx[id:%d]>" % userid, data.decode('UTF-8'))

        
    def ServiceHandler(userid, peername, reason):
        print("ServiceHandler[%d]>" % userid, peername, reason)
        pass    
    
    count = 0
    print("Initializing")
    try:
        netObj = []
        netObj.append(xNetTool("127.0.0.1", 7777, 1, RxHandlerServer, ServiceHandler, transport= "TCP", role= "server"))
        netObj.append(xNetTool("127.0.0.1", 7777, 2, RxHandler, ServiceHandler, transport= "TCP", role= "client"))
        netObj.append(xNetTool("127.0.0.1", 7778, 3, RxHandler, ServiceHandler, transport= "UDP", role= "client"))
        netObj.append(xNetTool("127.0.0.1", 7778, 4, RxHandlerServer, ServiceHandler, transport= "UDP", role= "server"))
        netObj.append(xNetTool("127.0.0.1", 7779, 5, RxHandler, ServiceHandler, transport= "UDP", role= "client"))
        netObj.append(xNetTool("127.0.0.1", 7780, 6, RxHandlerServer, ServiceHandler, transport= "UDP", role= "server"))
        while True:
            sleep(1)
            count = count + 1
            for obj in netObj:
                if obj.getRole() == "client":
                    obj.senddata(("data[%d]_form_[%s:%d]" % (count, obj.getHost(), obj.getPort())).encode('UTF-8'))
                    
            print(count)
    #------------------------------------------------------------------------------
    except KeyboardInterrupt:
        print('User Interrupted. Please wait...')
        logging.debug("---- CTRL^C -----")
        
    finally:
        for obj in netObj:
            obj.close()
        print("Deinitializing")
        
    #------------------------------------------------------------------------------
    print('Done')         
    logging.shutdown()

if __name__ == '__main__':
    democode()        
    