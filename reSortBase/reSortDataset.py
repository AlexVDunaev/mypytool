# -*- coding: utf-8 -*-
"""
Created on Wed Nov 10 16:51:25 2021

@author: Dunaev

2022.09.21
Скорректировал для случая одинаковых часовых поясов, (deltaTime=00000)
Для корректной работы под windows нужно скачать gzip

"""

import os
from time import sleep
from shutil import copyfile
import re

deltaTime = 00000  #70000 - это 7:00:00
dirPattern = re.compile('^new.(\d+).(\d+)')
daqFilePattern = re.compile('^new.(\d+).*.tsv') 
bagFilePattern = re.compile('^new.\d+.(\d+).(.*).bag') 
# bagFilePattern = re.compile('^new.\d+.(\d+).t25.bag') 

    
def copyBagFile(daqDir, bagDir, bagFile, season):
    result = bagFilePattern.findall(bagFile)
    if result:
        (part, extName) = result[0]
        newBagName = "new.%s.%s.%s.bag" % (season, part, extName)
        print("move file: %s / %s -> %s / %s" % (bagDir, bagFile, daqDir, newBagName))
        try:            
           # copyfile(bagDir+"\\"+bagFile, daqDir + "\\" + newBagName)
            os.rename(bagDir+"/"+bagFile, daqDir + "/" + newBagName)
        except OSError as err:
            print('extractFile error:', err)
    pass


def extractFile(daqDir):
    gzFilePattern = re.compile('^new.(\d+).*.gz$')
    for f in os.listdir(daqDir):
        result  = gzFilePattern.findall(f)
        if result:
            gzfile = daqDir+"/"+f
            print("gz file: %s" % (gzfile))
            # gunzip - удаляет архив. ничего не смог с этим поделать...
            try:            
                print("copy>  %s -> %s" % (gzfile, gzfile +".old"))
                copyfile(gzfile, gzfile +".old")    
                # os.system('gunzip -d %s' % (gzfile))
                # os.system('WinRAR x %s' % (gzfile))
                os.system('gzip -d %s' % (gzfile))
                 
                copyfile(gzfile +".old", gzfile)    
                os.remove(gzfile +".old")
            except OSError as err:
                print('extractFile error:', err)
    pass

def chackBagDir(folder):
    global bagFilePattern
    files = [f for f in os.listdir(folder) if bagFilePattern.findall(f)]
    return len(files) > 0

def chackDaqDir(folder):
    global daqFilePattern
    files = [f for f in os.listdir(folder) if daqFilePattern.findall(f)]
    return len(files) > 0


datasets = [f for f in os.listdir() if dirPattern.findall(f)]

pairFile = []


for f1 in datasets:
    (num1, season) = dirPattern.findall(f1)[0]
    # print(f1)
    for f2 in datasets:
        (num2, _) = dirPattern.findall(f2)[0]
        if abs(int(num1)+deltaTime - int(num2)) < 3:
            # print("DAQ time: %s -> bag time:%s" % (num1, num2))
            if chackDaqDir(f1) and chackBagDir(f2):
                pairFile.append([f1, f2, season])


for daqDir, bagDir, season in pairFile:
    print("DAQ dir: %s -> bag dir:%s" % (daqDir, bagDir))
    extractFile(daqDir)
    filelist = os.listdir(bagDir)
    for bagFile in filelist:
        copyBagFile(daqDir, bagDir, bagFile, season)
        # print("copy:", daqDir, bagDir, bagFile, season)
            

print("Done.")